fun main(){
    println(function1(2,4,6,8))
    println(function2("level"))
}

//davaleba 1

fun function1(vararg array : Int): Int{
//    var array = arrayOf(1,2,3,4)
    var sum = 0
    var num = 0;
    var avg = 0
    for(item in array){
        if (item % 2 == 0){
            sum+=item
            num++
            avg = sum / num
        }
    }
    return avg
}
//davaleba 2
fun function2(string1 : String) : Boolean{
    var stringRev = ""
    for(item in (string1.length) - 1 downTo 0){
        stringRev = stringRev + string1[item]
    }
    return string1.lowercase() == stringRev.lowercase()

}